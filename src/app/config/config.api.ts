import { environment } from './../../environments/environment.prod';

const BASE_URL: string = environment.baseAPIUrl;

export const APIBase: string = `${BASE_URL}/`;